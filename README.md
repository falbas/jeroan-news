# Jeroan News
Berita adalah laporan mengenai suatu peristiwa atau kejadian yang terbaru (aktual), laporan mengenai fakta-fakta yang aktual, menarik perhatian, dinilai penting, atau luar biasa. Di era modern ini orang-orang sudah meninggalkan untuk membaca berita di koran, banyak dari kita yang menggunakan handphone sebagai media untuk memperoleh berita. Oleh karena itu, kami merancang aplikasi berita online berbasis Android dengan Guardian API yang diimplementasikan dengan Android Studio. Bahasa pemrograman yang kami gunakan adalah java, dimana bahasa ini merupakan bahasa native dalam mengembangkan suatu aplikasi berbasis Android.

### Kelompok 2 (2IA01)
* M Wildan Alghifari (51420384)
* Farhan Alba Saputra (50420456)
* Lukman Nur Hakim (50420674)
* Jonathan Jordan (50420616)
* Windy Indriarnie (51420280)

### Persyaratan
Versi SDK Minimal = 21

### Cara Install
* Clone repositori ini
* Buka Android Studio pilih Open
* Cari project yang sudah diclone tadi kemudian pilih
* Setelah project berhasil dibuka, tunggu proses build gradle selesai
* Project siap digunakan

### Mengganti API Key
Jika ingin menggunakan API Key pribadi, dapatkan API Key terlebih dahulu di https://open-platform.theguardian.com/access/. Kemudian pada file `Constants.java` ubah pada bagian `API_KEY` dengan API Key milik anda.

```
public static final String API_KEY = "API_KEY_ANDA";
```

### Fitur
* Side Nav Bar
* Splash Screen
* Settings
* Web View

### Screenshot
![image](https://user-images.githubusercontent.com/77236568/176343715-7fe1eea9-f722-4e79-acb7-ad7ad2895e49.png)
![image](https://user-images.githubusercontent.com/77236568/176343740-3c6ec3ee-7ec7-412b-ae5b-02757a462e4d.png)
![image](https://user-images.githubusercontent.com/77236568/176343952-c19e2074-c335-4c36-8c9f-fde4795da016.png)
![image](https://user-images.githubusercontent.com/77236568/176343756-068c1424-4b81-47ec-8ed7-92c86dbb41c7.png)
![image](https://user-images.githubusercontent.com/77236568/176343765-9da222ae-e013-4940-8f72-db358fa27d1d.png)

### Source Code
https://github.com/sooshin/android-news-app

